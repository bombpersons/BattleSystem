#include <battle/battle.hpp>
#include <cstdio>
#include <cstdlib>

using namespace battle;

int main(int argc, char** argv) {
  Battle battle;

  DummyParticipant monster("Monster");
  monster.setSide(1);

  DummyParticipant player1("Player1");
  DummyParticipant player2("Player2");
  player1.setCooldown(0);

  battle.addParticipant(&monster);
  battle.addParticipant(&player1);
  battle.addParticipant(&player2);

  // Go through each turn.
  TurnOrder& to = battle.getTurnOrder();
  for (Participant* p = to.getNext(); p; p = to.getNext()) {
    DummyParticipant* dp = (DummyParticipant*)p;

    printf("---------- Monster cooldown: %i, Player 1 cooldown: %i, Player 2 cooldown: %i\n", monster.getCooldown(), player1.getCooldown(), player2.getCooldown());
    printf("%s used a move! Cooldown: %i\n", p->getName(), p->getCooldown());

    dp->setCooldown(rand() % 100);
  }

  return 0;
}
