#pragma once

#include <map>

namespace battle {
  class Participant;
  class Ability {
  public:
    Ability() {}
    virtual ~Ability() {}

    virtual const char* getAbilityName() const = 0;
    virtual void doAbility(Participant* caster, Participant** targets, unsigned int numTargets) const = 0;
  };

  class AbilityCache {
  public:
    AbilityCache() {}
    virtual ~AbilityCache() {}

    // Register abilities. (we don't want to have to make instances of this class for every participant)
    void registerAbility(Ability* ability, const char* name);
    Ability* getAbility(const char* name);

    private:
     std::map<const char*, Ability*> m_abilities;
  };

  class DummyAbility : public Ability {
  public:
    DummyAbility(const char* name) : m_name(name) {}
    virtual ~DummyAbility() {}

    const char* getAbilityName() const override { return m_name; }
    void doAbility(Participant* caster, Participant** targets, unsigned int numTargets) const override {}

  private:
    const char* m_name;
  };
}
