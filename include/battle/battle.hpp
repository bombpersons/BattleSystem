#pragma once

#include <battle/participant.hpp>
#include <battle/turnorder.hpp>

namespace battle {
  class AbilityCache;
  class Battle {
  public:
    Battle();
    virtual ~Battle();

    // Set the abilities that are available.
    void setAbilityCache(AbilityCache* cache) { m_abilities = cache; }

    // Get an object that manages the turn order of the battle.
    TurnOrder& getTurnOrder() { return m_turnOrder; }

    // Add / Remove participants.
    void addParticipant(Participant* participant);
    void removeParticipant(Participant* participant);

  private:
    // Object that determines the turn order.
    TurnOrder m_turnOrder;

    // Abilities that participants can use.
    AbilityCache* m_abilities;
  };

}
