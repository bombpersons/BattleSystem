#pragma once

/*
  A Participant is a something in a battle that can be damaged / attack.

  The interface defined here only cares about the information needed to
  calculate things happening in the battle.

  Any logic that calculates stats or hit points based on
  a characters level should be done outside of this library,
  simply have the functions below return the calculated values.
*/

namespace battle {

  class Participant {
  public:
    Participant() {}
    virtual ~Participant() {}

    // Name
    virtual const char* getName() const = 0;

    // Whose side are you on?
    virtual unsigned int getSide() const = 0;

    // Current HP, Max HP
    virtual unsigned int getMaxHP() const = 0;
    virtual unsigned int& getCurrentHP() = 0;

    // Current MP, Max MP
    virtual unsigned int getMaxMP() const = 0;
    virtual unsigned int& getCurrentMP() = 0;

    // Stats
    // Str, Dex, Con, Int, Wis, Cha
    virtual unsigned int getStr() const = 0;
    virtual unsigned int getDex() const = 0; // <- use this as a base to define turn speed?
                                             //    we probably want it to be based on the action taken too.
    virtual unsigned int getCon() const = 0;
    virtual unsigned int getInt() const = 0;
    virtual unsigned int getWis() const = 0;
    virtual unsigned int getCha() const = 0;

    // TODO: Should we have some way of damaging stats directly?
    //       Or are status effects sufficient?

    // Status Effects
    // TODO: Figure out system for storing status effects + whatever state they need.

    // Potential actions
    // TODO: Figure out how to store the actions a participant has available to them.

    // Items the participant has
    // TODO: Items should probably also have some sort of interface.
    //       At the very least we neeed some sort of way to define what an item
    //       would do in battle.

    // How long until this participant can act again?
    virtual unsigned int getCooldown() const = 0;
    virtual void setCooldown(unsigned int i) = 0;
  };

  class DummyParticipant : public Participant {
  public:
    DummyParticipant(const char* name) : m_name(name)
                                        ,m_side(0)
                                        ,m_maxHP(100)
                                        ,m_currentHP(100)
                                        ,m_maxMP(50)
                                        ,m_currentMP(50)
                                        ,m_str(10)
                                        ,m_dex(10)
                                        ,m_con(10)
                                        ,m_int(10)
                                        ,m_wis(10)
                                        ,m_cha(10)
                                        ,m_cooldown(10) {}
    virtual ~DummyParticipant() {}

    // Participant Interface
    const char* getName() const override { return m_name; }

    unsigned int getSide() const override { return m_side; }
    void setSide(unsigned int s) { m_side = s; }

    unsigned int getMaxHP() const override { return m_maxHP; }
    unsigned int& getCurrentHP() override { return m_currentHP; }

    unsigned int getMaxMP() const override { return m_maxMP; }
    unsigned int& getCurrentMP() override { return m_currentMP; }

    unsigned int getStr() const override { return m_str; }
    unsigned int getDex() const override { return m_dex; }
    unsigned int getCon() const override { return m_con; }
    unsigned int getInt() const override { return m_int; }
    unsigned int getWis() const override { return m_wis; }
    unsigned int getCha() const override { return m_cha; }

    unsigned int getCooldown() const override { return m_cooldown; }
    void setCooldown(unsigned int c) override { m_cooldown = c; }

  private:
    // Name
    const char* m_name;

    // Which team are we on?
    unsigned int m_side;

    // Hit points.
    unsigned m_maxHP, m_currentHP;
    unsigned m_maxMP, m_currentMP;

    // Stats
    unsigned int m_str, m_dex, m_con, m_int, m_wis, m_cha;

    unsigned int m_cooldown;
  };

}
