#pragma once

#include <battle/participant.hpp>

#include <list>

namespace battle {
  class TurnOrder {
  public:
    TurnOrder();
    virtual ~TurnOrder();

    // Add / Remove participants.
    void addParticipant(Participant* p);
    void removeParticipant(Participant* p);
    unsigned int getParticipantCount();

    // Get whose turn is next.
    Participant* getNext();

    // Get a forecast of the turn order.
    bool getForecast(Participant** list, unsigned int num);

  private:
    std::list<Participant*> m_participants;
  };
}
