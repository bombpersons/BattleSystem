#include <battle/ability.hpp>

#include <cassert>

namespace battle {
  void AbilityCache::registerAbility(Ability* ability, const char* name) {
    if (m_abilities.find(name) != m_abilities.end()) {
      assert("Duplicate ability! There shouldn't be more than one ability with the same name!");
      return;
    }

    m_abilities[name] = ability;
  }

  Ability* AbilityCache::getAbility(const char* name) {
    if (m_abilities.find(name) == m_abilities.end())
      return nullptr;
    return m_abilities[name];
  }
}
