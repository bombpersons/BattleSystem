#include <battle/battle.hpp>

#include <cstdio>

namespace battle {
  Battle::Battle() :
    m_abilities(nullptr) {
    printf("Battle created!\n");
  }

  Battle::~Battle() {

  }

  void Battle::addParticipant(Participant* participant) {
    m_turnOrder.addParticipant(participant);
  }
  void Battle::removeParticipant(Participant* participant) {
    m_turnOrder.removeParticipant(participant);
  }

}
