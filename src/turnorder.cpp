#include <battle/turnorder.hpp>

#include <cstdio>
#include <algorithm>

namespace battle {
  TurnOrder::TurnOrder() {

  }

  TurnOrder::~TurnOrder() {

  }

  void TurnOrder::addParticipant(Participant* p) {
    m_participants.push_back(p);
  }

  void TurnOrder::removeParticipant(Participant* p) {
    auto it = std::find(m_participants.begin(), m_participants.end(), p);
    if (it != m_participants.end()) {
      m_participants.erase(it);
    }
  }

  unsigned int TurnOrder::getParticipantCount() {
    return m_participants.size();
  }

  Participant* TurnOrder::getNext() {
    if (m_participants.empty())
      return nullptr;

    // Re-sort the list.
    m_participants.sort([](const Participant* p1, const Participant* p2) {
      return p1->getCooldown() < p2->getCooldown();
    });

    Participant* next = m_participants.front();

    // Reduce all of the participants cooldowns by the amount of the chosen participant.
    unsigned int usedTime = next->getCooldown();
    for (auto& p : m_participants) {
      p->setCooldown(p->getCooldown() - usedTime);
    }

    return next;
  }

  bool TurnOrder::getForecast(Participant** list, unsigned int num) {
    // TODO: Bunch of ways we can do this.
    //       1. We can use the participants base speed to guess at what their cooldowns will be.
    //       2. Might be some way we can ask the game for a prediction of their moves.

    return false;
  }
}
