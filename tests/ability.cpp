#include <battle/ability.hpp>
using namespace battle;

#include "catch2.hpp"

TEST_CASE("Abilities can be registered and retrieved.", "[abilities]") {
  AbilityCache cache;
  DummyAbility attack("Attack");

  SECTION("Adding an attack can be retrieved.") {
    const char* attackName = "Attack";
    REQUIRE(cache.getAbility(attackName) == nullptr);
    cache.registerAbility(&attack, attackName);
    REQUIRE(cache.getAbility(attackName) == &attack);
  }
}
