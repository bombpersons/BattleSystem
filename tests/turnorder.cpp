#include <battle/participant.hpp>
#include <battle/turnorder.hpp>
using namespace battle;

#define CATCH_CONFIG_MAIN
#include "catch2.hpp"

TEST_CASE("TurnOrder can add and remove participants.", "[turnorder]") {
  TurnOrder turnorder;
  REQUIRE(turnorder.getParticipantCount() == 0);

  DummyParticipant dummy("Dummy");

  SECTION("Adding / Removing a participant changes size.") {
    turnorder.addParticipant(&dummy);
    REQUIRE(turnorder.getParticipantCount() == 1);
    turnorder.removeParticipant(&dummy);
    REQUIRE(turnorder.getParticipantCount() == 0);
  }
}

TEST_CASE("TurnOrder picks the correct participant for the next turn.", "[turnorder]") {
  TurnOrder turnorder;

  DummyParticipant dummy1("Dummy1");
  DummyParticipant dummy2("Dummy2");
  DummyParticipant dummy3("Dummy3");

  turnorder.addParticipant(&dummy1);
  turnorder.addParticipant(&dummy2);
  turnorder.addParticipant(&dummy3);

  SECTION("Turn order is correct.") {
    dummy1.setCooldown(10);
    dummy2.setCooldown(2);
    dummy3.setCooldown(5);
    REQUIRE(turnorder.getNext() == &dummy2);
    dummy2.setCooldown(5);

    REQUIRE(turnorder.getNext() == &dummy3);
    dummy3.setCooldown(20);

    REQUIRE(turnorder.getNext() == &dummy2);
    dummy2.setCooldown(19);

    REQUIRE(turnorder.getNext() == &dummy1);
  }

  turnorder.removeParticipant(&dummy1);
  turnorder.removeParticipant(&dummy2);
  turnorder.removeParticipant(&dummy3);
}
